import React from "react";
import Stage from "./../stage";

const App = () => {
	return (
		<div style={{ width: "100vw", height: "100vh" }}>
			<Stage />
		</div>
	);
};

export default App;
