import { animated, useSpring } from "@react-spring/three";
import { useFrame } from "@react-three/fiber";
import React, { useRef, useState } from "react";

const Box = (props: JSX.IntrinsicElements["mesh"]) => {
	const mesh = useRef<THREE.Mesh>(null!);
	const [hovered, setHover] = useState(false);
	const [active, setActive] = useState(false);
	useFrame((state, delta) => (mesh.current.rotation.x += 0.01));
	const { scale } = useSpring({ scale: active ? 1.5 : 1 });
	return (
		<animated.mesh
			{...props}
			ref={mesh}
			scale={scale}
			onClick={(event) => setActive(!active)}
			onPointerOver={(event) => setHover(true)}
			onPointerOut={(event) => setHover(false)}
		>
			<boxGeometry args={[2, 2, 2]} />
			<meshPhongMaterial color={hovered ? "red" : "gray"} />
		</animated.mesh>
	);
};

export default Box;
