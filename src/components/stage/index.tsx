import { Canvas } from "@react-three/fiber";
import React from "react";
import Box from "./../box";

const Stage = () => (
	<Canvas>
		<ambientLight intensity={0.5} />
		<pointLight position={[10, 10, 10]} />
		<Box position={[-4, 0, 0]} />
		<Box position={[4, 0, 0]} />
	</Canvas>
);

export default Stage;
